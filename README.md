# Ansible Role: PostgreSQL


Installs and configures PostgreSQL.

## Requirements

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook:

    - hosts: database
      become: yes
      roles:
        - role: postgresql

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    postgresql_restarted_state: "restarted"

Set the state of the service when configuration changes are made. Recommended values are `restarted` or `reloaded`.

    postgresql_python_library: python3-psycopg2

Library used by Ansible to communicate with PostgreSQL. If you are using Python 3.

    postgresql_user: postgres
    postgresql_group: postgres

The user and group under which PostgreSQL will run.

    postgresql_unix_socket_directories:
      - /var/run/postgresql

The directories (usually one, but can be multiple) where PostgreSQL's socket will be created.

    postgresql_service_state: started
    postgresql_service_enabled: true

Control the state of the postgresql service and whether it should start at boot time.

    postgresql_global_config_options:
      - option: unix_socket_directories
        value: '{{ postgresql_unix_socket_directories | join(",") }}'
      - option: log_directory
        value: 'log'
Global configuration options that will be set in `postgresql.conf`.
For PostgreSQL versions older than 9.3 you need to at least override this variable and set the `option` to `unix_socket_directory`.
If you override the value of `option: log_directory` with another path, relative or absolute, then this role will create it for you. 

    postgresql_hba_entries:
      - { type: local, database: all, user: postgres, auth_method: peer }
      - { type: local, database: all, user: all, auth_method: peer }
      - { type: host, database: all, user: all, address: '127.0.0.1/32', auth_method: md5 }
      - { type: host, database: all, user: all, address: '::1/128', auth_method: md5 }

Configure [host based authentication](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html) entries to be set in the `pg_hba.conf`. Options for entries include:

  - `type` (required)
  - `database` (required)
  - `user` (required)
  - `address` (one of this or the following two are required)
  - `ip_address`
  - `ip_mask`
  - `auth_method` (required)
  - `auth_options` (optional)

If overriding, make sure you copy all of the existing entries from `defaults/main.yml` if you need to preserve existing entries.

    postgresql_locales:
      - 'en_US.UTF-8'

Used to generate the locales used by PostgreSQL databases.

    postgresql_databases:
      - name: aisks # required; the rest are optional
        #   lc_collate: # defaults to 'en_US.UTF-8'
        #   lc_ctype: # defaults to 'en_US.UTF-8'
        #   encoding: # defaults to 'UTF-8'
        #   template: # defaults to 'template0'
        #   login_host: # defaults to 'localhost'
        login_password: 123456789 # defaults to not set
        #   login_user: developer # defaults to '{{ postgresql_user }}'
        #   login_unix_socket: # defaults to 1st of postgresql_unix_socket_directories
        #   port: # defaults to not set
        #   owner: developer # defaults to postgresql_user
        #   state: # defaults to 'present'


A list of databases to ensure exist on the server. Only the `name` is required; all other properties are optional.

    postgresql_users: []
    # - name: username #required; the rest are optional
    #   password: # defaults to not set
    #   encrypted: # defaults to not set
    #   priv: # defaults to not set
    #   role_attr_flags: # defaults to not set
    #   db: # defaults to not set
    #   login_host: # defaults to 'localhost'
    #   login_password: # defaults to not set
    #   login_user: # defaults to '{{ postgresql_user }}'
    #   login_unix_socket: # defaults to 1st of postgresql_unix_socket_directories
    #   port: # defaults to not set
    #   state: # defaults to 'present'
 
A list of users to ensure exist on the server. Only the `name` is required; all other properties are optional.

    postgres_users_no_log: true

## Dependencies

None.

## Example Playbook

    - hosts: db
      become: yes
      roles:
        - postgresql

## License

MIT / BSD

## Author Information

Leonid Iakovlev <dvps.dungeon@gmail.com>
